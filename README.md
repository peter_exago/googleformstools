# README #

### What is this repository for? ###

This repository contains scripts useful for manipulating data from Google Forms into reportable formats.

### How do I get set up? ###

You need Python to run these scripts.

### Who do I talk to? ###

Talk to Peter for questions

### googleFormsNormalizer.py ###
This script takes an input CSV file with form questions as columns, and outputs to a CSV where each response is its own record.

    Input example:
    Timestamp, question1, question2, question3
    8/1/2017 14:31:19, answera, answerb, answerc
    8/1/2017 17:00:43, answerd, answere, answerf
    ...

    Output example
    Timestamp, question, response
    8/1/2017 14:31:19, question1, answera
    8/1/2017 14:31:19, question2, answerb
    8/1/2017 14:31:19, question3, answerc
    8/1/2017 17:00:43, question1, answerd
    8/1/2017 17:00:43, question2, answere
    8/1/2017 17:00:43, question3, answerf
    ...