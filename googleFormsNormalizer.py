import csv
import sys

def loadDataFromCsv():
	inFilePath = sys.argv[1]
	with open(inFilePath, "r", encoding="utf-8") as inFile:
		reader = csv.reader(inFile)
		
		headerRow = []
		data = []
		
		for row in reader:
			data.append(row)

		return data

if len(sys.argv) < 2:
	print("Please provide the input CSV file")
	sys.exit(1)

data = loadDataFromCsv()

headerRow = data[0]
data = data[1:]
transformedData = [["Timestamp", "Question", "Response"]]

for row in data:
	for col in range(1, len(row)):
		outputRow = []
		
		outputRow.append(row[0]) # Add the timestamp
		outputRow.append(headerRow[col]) # Add the question
		outputRow.append(row[col]) # Add the response
		
		transformedData.append(outputRow)
		
with open("output.csv", "w", newline="\n", encoding="utf-8") as outFile:
	writer = csv.writer(outFile, lineterminator='\r\n')
	writer.writerows(transformedData)